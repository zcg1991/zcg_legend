<?php
namespace Zcg\Legend;

class BitmapData{
    public $path;//原图路径或数据流
    public $width;//原图宽
    public $height;//原图高
    public $context;//承载图片原信息的画布,包含完整的bit流
    public $type = 'png';

    //计算属性：即原图中截取一块作为Bitmap显示对象的内容
    public $clip_x = 0;//剪取内容的起点x坐标
    public $clip_y = 0;//剪取内容的起点y坐标
    public $clip_width;//剪取内容的宽度
    public $clip_height;//剪取内容的高度

    private $allow_mime_types = ['jpeg','png','bmp','gif','jpg'];

    public function __construct($path,$x=0,$y=0,$w=0,$h=0)
    {
        $this->path = $path;
        if(strstr($this->path,'http')){
            $this->type = 'string';
            //if(strstr($this->path,'https')){
                $ch=curl_init();
                $timeout=20;
                curl_setopt($ch,CURLOPT_URL,$path);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
                $this->path=curl_exec($ch);
                curl_close($ch);
//                $options = [
//                    "ssl" => [
//                        "verify_peer"=>false,
//                        "verify_peer_name"=>false,
//                    ],
//                ];
//                $this->path = file_get_contents($path,false, stream_context_create($options));
//            }else{
//                $this->path = file_get_contents($path);
//            }
        }else{
            $arr = explode('.',$path);
            $this->type = $arr[count($arr)-1];

            if(!in_array($this->type,$this->allow_mime_types)){ //接受数据流作为数据源 如 application/octet-stream类型的数据流
                $this->type = 'string';
            }

            if($this->type == 'jpg'){
                $this->type = 'jpeg';
            }
        }

        $func = 'imagecreatefrom'.$this->type;
        try{
            $this->context = $func($this->path);
        }catch (\Throwable $t){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('无效的图片路径:'.$this->path);
            }
            throw new \Exception('无效的图片路径:'.$this->path);
        }

        $this->width = imagesx($this->context);
        $this->height = imagesy($this->context);
        $this->clip_x = $x;
        $this->clip_y = $y;
        $this->clip_width = $w?:$this->width;
        $this->clip_height = $h?:$this->height;
        if(!$this->height || !$this->width){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('必须传入一个有效的图片');
            }
            throw new \Exception('必须传入一个有效的图片');
        }
    }

    public function getXPixel($x)
    {
        return imagecolorat($this->context, $x, $this->clip_height-2);
    }

    public function getYPixel($y)
    {
        return imagecolorat($this->context, $this->clip_width - 2, $y);
    }

    //获取主色
    public function getMainColor()
    {
        $rColorNum=$gColorNum=$bColorNum=$total=0;
        for ($x=0;$x<$this->clip_width;$x++) {
            for ($y=0;$y<$this->clip_height;$y++) {
                $rgb = imagecolorat($this->context,$x,$y);
                //三通道
                $r = ($rgb >> 16) & 0xFF;
                $g = ($rgb >> 8) & 0xFF;
                $b = $rgb & 0xFF;
                $rColorNum += $r;
                $gColorNum += $g;
                $bColorNum += $b;
                $total++;
            }
        }
        $rgb = array();
        $r = round($rColorNum/$total);
        $g = round($gColorNum/$total);
        $b = round($bColorNum/$total);
        return [$r,$g,$b];
    }

}
