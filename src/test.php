<?php
ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');

error_reporting(E_ALL);
date_default_timezone_set('Asia/Shanghai');

! defined('BASE_PATH') && define('BASE_PATH', dirname(__DIR__, 1));

include 'Functions.php';
spl_autoload_register(function ($class) {
    $prefix = 'Zcg\\Legend\\';
    $base_dir = __DIR__.DIRECTORY_SEPARATOR;

    // 判断类的前缀是不是上面的命名空间前缀
    $len = strlen($prefix);
    if (strncmp($prefix, $class, $len) !== 0) {
        // 不是，则交给其他类加载器去加载，不使用PSR4
        return;
    }

    // 获取相对的命名空间
    $relative_class = substr($class, $len);

    $file = $base_dir . str_replace('\\', '/', $relative_class) . '.php';

    // 如果该类存在，则require它
    if (file_exists($file)) {
        require_once $file;
    }
});

//$stage = new Zcg\Legend\Stage(600,600,'white',0);
//$text = new Zcg\Legend\Display\Text('张晨光游戏引擎2.0');
//$stage->add_child($text);
//$text->bottomCenter();
//$text2 = new Zcg\Legend\Display\Text('张晨光游戏引擎2.0');
//$stage->add_child($text2);
//$text2->topCenter();
//$stage->save("./test");
//zMerge(['https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png',
//    'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png']);

//verify_code();

//$qrcode = new \Zcg\Legend\Display\Qrcode('888',0,5,2);
//echo $qrcode->getFile();

//echo zQrcode('hello big',true,'C:\Users\zcg\Desktop\work\zcg_legend\runtime\images\159799120840739.png');
//echo zQrcode('hello big',true);


//echo zBarcode(123456);

$compress = new \Zcg\Legend\App\Compress('https://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTI6BKsVQhI5MZZOoNia6daecFVmRclPMV1qicYkUHbT9vOia7jiaF44f2y5phltnC5vjuoiclNXk9mwUvQ/132');//C:\Users\zcg\Desktop\kl.png
//echo $compress->ellipse().PHP_EOL;
echo $compress->circle().PHP_EOL;
//
//echo $compress->widthClip(50).PHP_EOL;
//echo $compress->widthScale(50).PHP_EOL;
//
//echo $compress->heightClip(100).PHP_EOL;
//echo $compress->heightScale(100).PHP_EOL;

//$synthesis = new \Zcg\Legend\App\Synthesis();
//$synthesis->add_image('C:\Users\zcg\Desktop\bg.jpg')->add_empty(100);
//    ->add_image('C:\Users\zcg\Desktop\baidu.png')
//    ->add_image('C:\Users\zcg\Desktop\baidu.png');
//echo $synthesis->vertical('extend').PHP_EOL;
//echo $synthesis->horizontal('green');

//$water = new \Zcg\Legend\App\Water('C:\Users\zcg\Desktop\bg.jpg');
//echo $water->add_image('C:\Users\zcg\Desktop\baidu.png')
//    ->add_text('落1花2秋3辞4云5淡风轻','middle_left','yellow',30,[50,0],0,['line_width'=>200])
//    ->add_text('超然物外','top_center','white',30,[0,20],400)
//    ->add_text('落花秋辞','middle_left','yellow',30,[50,0],-1,'','vertical')
//    ->add_text('云淡风清','middle_right','yellow',30,[-50,0],-1,'msyh','vertical')
    //->add_text('云淡风清','right','yellow',30,[-50,0],0,'vertical')
//        ->add_line(0,0,400,100,'red')
//    ->save();

//zCode();

//var_dump(color_reverse([23,190,7]));
