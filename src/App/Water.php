<?php

namespace Zcg\Legend\App;

use Zcg\Legend\BitmapData;
use Zcg\Legend\Display\Bitmap;
use Zcg\Legend\Display\Text;
use Zcg\Legend\Stage;
use Lbxds\Job\DeleteFileJob;

class Water
{
    protected $background;
    protected $stage;

    public function __construct($background)
    {
        $this->background = $background;
        $bitmap_data = new BitmapData($this->background);
        $bitmap = new Bitmap($bitmap_data);
        $this->stage = new Stage($bitmap->width,$bitmap->height);
        $this->stage->add_child($bitmap);
    }

    public function add_image($image,$origin='center',$coordinate=[])
    {
        $bd = new BitmapData($image);
        $bitmap = new Bitmap($bd);
        $bitmap->setOrigin($origin);
        if($coordinate){
            $bitmap->setCoordinate($coordinate[0]??0,$coordinate[1]??0);
        }
        $this->stage->add_child($bitmap);
        return $this;
    }

    public function add_text($words,$origin='center',$color='black',$size=24,$coordinate=[],$equal_spacing=0,$config=[])
    {
        $font = $config['font'] ?? '';
        $direction = $config['direction'] ?? '';


        $text = new Text($words,$color,$size,0,$font,'',$config['line_width']??0,$config['weight']??0,$config['space']??null,$config['line_height']??0);
        $text->setOrigin($origin);
        if($coordinate){
            $text->setCoordinate($coordinate[0]??0,$coordinate[1]??0);
        }
        if($direction){
            $text->setDirection($direction);
        }

        //0表示不用等间隔处理,-1表示相对于整个画布等间隔处理
        if($equal_spacing) {
            $text->equalSpacing($equal_spacing);
        }

        if(isset($config['line']) && $config['line']){
            $text->graphic->line([0, -$text->height/2],$config['line']?:'black',1,$text->width + 30);
        }

        $this->stage->add_child($text);
        return $this;
    }

    public function add_line($x1=0,$y1=0,$x2=0,$y2=0,$color='black',$thickness=1)
    {
        $this->stage->graphic->line([$x1,$y1,$x2,$y2],$color,$thickness);
        return $this;
    }

    public function save()
    {
        $file = $this->stage->save('water_');
        deliver(new DeleteFileJob($file),300);
        return $file;
    }
}
