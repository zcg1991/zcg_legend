<?php

namespace Zcg\Legend\App;

use Zcg\Legend\BitmapData;
use Zcg\Legend\Display\Bitmap;
use Zcg\Legend\Stage;
use Lbxds\Job\DeleteFileJob;

/**
 * Class Synthesis
 * 图片合成
 */
class Synthesis
{
    protected $images = [];
    protected $bitmap_datas = [];
//    protected $direction;//vertical horizontal
//    protected $scale_mode;//min:按最小尺寸缩放对齐,max:按最大尺寸缩放对齐

    public $total_width = 0;//use in horizontal direction
    public $total_height = 0;//use in vertical direction
    public $min_width = 0;
    public $max_width = 0;
    public $min_height = 0;
    public $max_height = 0;

    //尾部增加的空白长度
    public $empty_length = 0;

    public function add_image($image)
    {
        $this->images[] = $image;
        $bd = new BitmapData($image);
        $this->total_height += $bd->height;
        $this->total_width += $bd->width;
        if($this->min_height == 0){
            $this->min_height = $bd->height;
        }else{
            $this->min_height = min($this->min_height,$bd->height);
        }
        if($this->max_height == 0){
            $this->max_height = $bd->height;
        }else{
            $this->max_height = max($this->max_height,$bd->height);
        }
        if($this->min_width == 0){
            $this->min_width = $bd->width;
        }else{
            $this->min_width = min($this->min_width,$bd->width);
        }
        if($this->max_width == 0){
            $this->max_width = $bd->width;
        }else{
            $this->max_width = max($this->max_width,$bd->width);
        }
        $this->bitmap_datas[] = $bd;
        return $this;
    }

    public function add_empty($length)
    {
        if($length < 1) {
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('长度必传');
            }
            throw new \Exception('长度必传');
        }
        $this->empty_length = $length;
        return $this;
    }

    public function vertical($bg_color='white',$scale_mode='min')
    {
        if($scale_mode === 'min'){
            $width = $this->min_width;
        }else{
            $width = $this->max_width;
        }
        $height = $this->total_height + $this->empty_length;

        //继承最后一行底色
        $extend_mode = '';
        if($bg_color === 'extend' || $bg_color === 'main') {
            $extend_mode = $bg_color;
            $bg_color = 'white';
        }

        $stage = new Stage($width,$height,$bg_color);
        $current_h = 0;
        $last_bd = null;
        foreach ($this->bitmap_datas as $bd)
        {
            $w = $width;
            $h = $bd->height * $w / $bd->width;
            $bitmap = new Bitmap($bd,0,0,$w,$h);
            $bitmap->setY($current_h);
            $stage->add_child($bitmap);
            $current_h += $bitmap->height;
            $last_bd = $bd;
        }

        //继承最后一个bd的颜色
        if($last_bd && $extend_mode){
            if($extend_mode === 'main'){
                $rgbColor = $stage->getColor($last_bd->getMainColor());
            }

            for ($x = 0; $x < $width; $x++) {
                if($extend_mode === 'extend'){
                    $rgbColor =$last_bd->getXPixel($x);
                }
                for ($y = $current_h; $y < $height; $y++) {
                    imagesetpixel($stage->context, $x, $y, $rgbColor);
                }
            }
        }

        $file = $stage->save('synthesis_');
        deliver(new DeleteFileJob($file),300);
        return $file;
    }

    public function horizontal($bg_color='white',$scale_mode='min')
    {
        if($scale_mode === 'min'){
            $height = $this->min_height;
        }else{
            $height = $this->max_height;
        }
        $width = $this->total_width + $this->empty_length;
//        if($this->bitmap_datas  && $bg_color === 'extend'){
//            $bitmap_data = end($this->bitmap_datas);
//            $bg_color = $bitmap_data->getLastPixel();
//        }

        //继承最后一行底色
        $extend_mode = '';
        if($bg_color === 'extend' || $bg_color === 'main') {
            $extend_mode = $bg_color;
            $bg_color = 'white';
        }

        $stage = new Stage($width,$height,$bg_color);
        $current_w = 0;
        $last_bd = null;
        foreach ($this->bitmap_datas as $bd)
        {
            $h = $height;
            $w = $bd->width * $h / $bd->height;
            $bitmap = new Bitmap($bd,0,0,$w,$h);
            $bitmap->setX($current_w);
            $stage->add_child($bitmap);
            $current_w += $bitmap->width;
            $last_bd = $bd;
        }

        //继承最后一个bd的颜色
        if($last_bd && $extend_mode){
            if($extend_mode === 'main'){
                $rgbColor = $stage->getColor($last_bd->getMainColor());
            }

            for ($x = $current_w; $x < $width; $x++) {
                for ($y = 0; $y < $height; $y++) {
                    if($extend_mode === 'extend'){
                        $rgbColor =$last_bd->getYPixel($y);
                    }
                    imagesetpixel($stage->context, $x, $y, $rgbColor);
                }
            }
        }
        $file = $stage->save('synthesis_');
        deliver(new DeleteFileJob($file),300);
        return $file;
    }

}