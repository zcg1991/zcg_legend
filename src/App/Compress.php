<?php

namespace Zcg\Legend\App;

use Zcg\Legend\BitmapData;
use Zcg\Legend\Display\Bitmap;
use Zcg\Legend\Stage;
use Lbxds\Job\DeleteFileJob;

/**
 * Class Compress
 * 图片缩放
 */
class Compress
{
    public $image;
    public $pattern;
    public $width;
    public $height;
    public $bitmap_data;
    //圆
    public $radius;
    //标准椭圆
    public $x_radius;
    public $y_radius;

    public function __construct($image)
    {
        $this->image = $image;
        $this->bitmap_data = new BitmapData($this->image);
    }

    //等宽缩放
    public function widthScale($width)
    {
        if(intval($width) < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('width必须大于0');
            }
            throw new \Exception('width必须大于0');
        }
        $this->restore();
        $this->pattern = 'width_scale';
        $this->width = $width;
        $this->height = $this->bitmap_data->height * $this->width / $this->bitmap_data->width;
        return $this->save();
    }

    //等高缩放
    public function heightScale($height)
    {
        if(intval($height) < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('height必须大于0');
            }
            throw new \Exception('height必须大于0');
        }
        $this->restore();
        $this->pattern = 'height_scale';
        $this->height = $height;
        $this->width = $this->bitmap_data->width * $this->height / $this->bitmap_data->height;
        return $this->save();
    }

    //manual scale:指定缩放
    public function scale(int $width,int $height)
    {
        if($height < 1 || $width < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('参数错误');
            }
            throw new \Exception('参数错误');
        }
        $this->restore();
        $this->pattern = 'scale';
        $this->width = $width;
        $this->height = $height;
        return $this->save();
    }

    public function widthClip(int $width,int $x=0,int $y=0)
    {
        if(intval($width) < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('width必须大于0');
            }
            throw new \Exception('width必须大于0');
        }
        $this->restore();
        $this->pattern = 'width_clip';
        $this->width = $width;
        $this->bitmap_data->clip_width = $width;
        $height = $this->bitmap_data->height * $width / $this->bitmap_data->width;
        $this->height = $height;
        $this->bitmap_data->clip_height = $height;
        $this->bitmap_data->clip_x = $x;
        $this->bitmap_data->clip_y = $y;
        return $this->save();
    }

    public function heightClip(int $height,int $x=0,int $y=0)
    {
        if($height < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('参数错误');
            }
            throw new \Exception('参数错误');
        }
        $this->restore();
        $this->pattern = 'height_clip';
        $this->height = $height;
        $this->bitmap_data->clip_height = $height;
        $width = $this->bitmap_data->width * $height / $this->bitmap_data->height;
        $this->width = $width;
        $this->bitmap_data->clip_width = $width;
        $this->bitmap_data->clip_x = $x;
        $this->bitmap_data->clip_y = $y;
        return $this->save();
    }

    public function clip(int $width,int $height,int $x=0,int $y=0)
    {
        if($height < 1 || $width < 1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('参数错误');
            }
            throw new \Exception('参数错误');
        }
        $this->restore();
        $this->pattern = 'clip';
        $this->height = $height;
        $this->bitmap_data->clip_height = $height;
        $this->width = $width;
        $this->bitmap_data->clip_width = $width;
        $this->bitmap_data->clip_x = $x;
        $this->bitmap_data->clip_y = $y;
        return $this->save();
    }

    public function circle($radius = 0)
    {
        $this->restore();
        $this->radius = $radius;
        $this->pattern = 'circle';
        return $this->save();
    }

    public function ellipse($width=0,$height=0)
    {
        $this->restore();
        $this->x_radius = $width/2;
        $this->y_radius = $height/2;
        $this->pattern = 'ellipse';
        return $this->save();
    }

    //复位操作
    protected function restore()
    {
        $this->bitmap_data->clip_width = $this->bitmap_data->width;
        $this->bitmap_data->clip_height = $this->bitmap_data->height;
        $this->bitmap_data->clip_x = 0;
        $this->bitmap_data->clip_y = 0;
        $this->width = $this->bitmap_data->width;
        $this->height = $this->bitmap_data->height;
    }

    protected function save()
    {

        if($this->pattern === 'circle') {

            $radius = min($this->width,$this->height) / 2;
            if($this->radius && $this->radius < $radius) {
                $radius = $this->radius;
            }
            $bp = new Bitmap($this->bitmap_data,0,0,$this->width,$this->height);
            $stage = new Stage(2*$radius,2*$radius,'white',127);//创建一个圆的外切矩形画布

            //计算外切矩形坐标 与 原bitmap坐标的偏移
            $delta_x = ($bp->width - 2*$radius)/2;
            $delta_y = ($bp->height - 2*$radius)/2;

            $r_x = $this->width / 2;
            $r_y = $this->height / 2;
            for ($x = 0; $x < $this->width; $x++) {
                for ($y = 0; $y < $this->height; $y++) {
                    if (((($x - $r_x) * ($x - $r_x) + ($y - $r_y) * ($y - $r_y)) <= ($radius * $radius))) {
                        $rgbColor = imagecolorat($this->bitmap_data->context, $x, $y);
                        imagesetpixel($stage->context, $x-$delta_x, $y-$delta_y, $rgbColor);
                    }
                }
            }
        }else if($this->pattern === 'ellipse'){
            $bp = new Bitmap($this->bitmap_data,0,0,$this->width,$this->height);

            $r_x = $this->width / 2;
            $r_y = $this->height / 2;
            if($this->x_radius == 0 && $this->y_radius == 0){
                $x_radius = $this->width / 2;
                $y_radius = $this->height / 2;
            }else if($this->x_radius == 0){
                $y_radius = $this->height / 2;
                $x_radius = $this->width * $y_radius / $this->height;
            }else if($this->y_radius == 0){
                $x_radius = $this->width / 2;
                $y_radius = $this->height * $x_radius / $this->width;
            }else{
                $x_radius = $this->x_radius;
                $y_radius = $this->y_radius;
            }

            $stage = new Stage(2*$x_radius,2*$y_radius,'white',60);//创建一个椭圆的外切矩形画布
            //计算外切矩形坐标 与 原bitmap坐标的偏移
            $delta_x = ($bp->width - 2*$x_radius)/2;
            $delta_y = ($bp->height - 2*$y_radius)/2;

            for ($x = 0; $x < $this->width; $x++) {
                for ($y = 0; $y < $this->height; $y++) {
                    $rgbColor = imagecolorat($this->bitmap_data->context, $x, $y);
                    if (  ($x - $r_x) * ($x - $r_x) /($x_radius*$x_radius)+ ($y - $r_y) * ($y - $r_y)/($y_radius*$y_radius)  <= 1 ) {
                        imagesetpixel($stage->context, $x-$delta_x, $y-$delta_y, $rgbColor);
                    }
                }
            }
        }else{
            $bp = new Bitmap($this->bitmap_data,0,0,$this->width,$this->height);
            $stage = new Stage($bp->width,$bp->height,'white');
            $stage->add_child($bp);
        }

        $file = $stage->save('compress_');
//        deliver(new DeleteFileJob($file),300);
        return $file;
    }

}
