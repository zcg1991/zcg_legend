<?php
namespace Zcg\Legend;

use Zcg\Legend\Display\Display;

class Sprite extends Display {
    use Canvas;

    public function __construct($width,$height,$background='white',$alpha=0)
    {
        parent::__construct();
        $this->width = $width;
        $this->height = $height;
        $this->context = imagecreatetruecolor($width, $height);
        imagefill($this->context,0,0,$this->getColor($background,$alpha));
        $this->type = 'sprite';
    }
}