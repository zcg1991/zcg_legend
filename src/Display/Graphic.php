<?php
namespace Zcg\Legend\Display;

use Zcg\Legend\Display\Display;

/**
 * Class Graphic
 * 图形可显示对象必须属于一个Stage或者Sprite
 * parent用于计算该画笔的相对起始落脚点位置
 */
class Graphic extends Display{
    public $color;
    public $alpha = 1;
    public $bitmap = null;
    public $set_list = [];
    public $parent = null;

    public function __construct($parent=null,$color='black')
    {
        $this->parent = $parent;
        $this->color = $color;
    }

    public function line($points,$color='',$thickness=1,$length=0)
    {
        var_dump($length);
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$points,$thickness,$length){
            //有$parent时取相对于（一个可现实对象，没有画布的）父级的位置,Graphic的width和height都为0，当作一个点来处理去获取画笔起点
            $this->setStartPoint($this->parent?:$stage);
            $x1 = $this->stage_x + $this->x + $points[0];
            $y1 = $this->stage_y + $this->y + $points[1];
            if($length){
                $x2 = $x1 + $length;
                $y2 = $y1;
            }else{
                $x2 = $this->stage_x + $this->x + $points[2];
                $y2 = $this->stage_y + $this->y + $points[3];
            }

            // imagesetthickness($stage->context, $thickness);
            // imageline($stage->context, $x1, $y1, $x2, $y2, $stage->getColor($color));

            if($color == 'white_red'){
                $w   = imagecolorallocate($stage->context, 255, 255, 0);
                $red = imagecolorallocate($stage->context, 255, 0, 0);

                /* 画一条虚线，5 个红色像素，5 个白色像素 */
                $style = array($red, $red, $red, $red, $red, $w, $w, $w, $w, $w);
                //imagesetstyle() 设定所有画线的函数（例如 imageline() 和 imagepolygon()）在使用特殊颜色 IMG_COLOR_STYLED 或者用 IMG_COLOR_STYLEDBRUSHED 画一行图像时所使用的风格。
                imagesetstyle($stage->context, $style);
            }

            if($thickness == 1){
                imageline($stage->context, $x1, $y1, $x2, $y2, $stage->getColor($color));

            }else if($x1 == $x2 || $y1 == $y2){
                $t = $thickness / 2 - 0.5;
                imagefilledrectangle($stage->context, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $stage->getColor($color));
            }else{
                $t = $thickness / 2 - 0.5;
                $k = ($y2-$y1) / ($x2-$x1);
                $a = $t / sqrt(1 + pow($k,2));
                $points = [
                    round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
                    round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
                    round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
                    round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
                ];
                imagefilledpolygon($stage->context, $points, 4, $stage->getColor($color));
            }
        };
    }

    public function circle($centre,$radius=100,$color='',$thickness=1)
    {
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$centre,$thickness,$radius){
            $this->setStartPoint($this->parent?:$stage);
            $cx = $this->stage_x + $this->x + $centre[0];
            $cy = $this->stage_y + $this->y + $centre[1];
            $width = $radius;
            $height = $radius;
            $start_angle = 0;
            $end_angle = 360;
            imagesetthickness($stage->context, $thickness);
            imagearc($stage->context, $cx, $cy, $width, $height, $start_angle, $end_angle, $stage->getColor($color));
        };
    }

    public function circlesIn($scope=[],$quantity=100,$color='random')
    {
        if(!$scope){
            $scope = [
                $this->stage_x,
                $this->stage_y,
                $this->parent->width,
                $this->parent->height
            ];
        }

        //$scop = [x,y,w,h]
        $this->set_list[] = function($stage) use($scope,$quantity){
            $this->setStartPoint($this->parent?:$stage);
            if($this->parent){
                $x1 = $this->stage_x + $this->x + $scope[0];
                $y1 = $this->stage_y + $this->y + $scope[1];
            }else{
                $x1 = $this->x + $scope[0];
                $y1 = $this->y + $scope[1];
            }

            $x2 = $x1 + $scope[2];
            $y2 = $y1 + $scope[3];
            for($i=0;$i<$quantity;$i++){
                imagearc($stage->context, mt_rand($x1,$x2), mt_rand($y1,$y2), 10, 10, 0, 360, $stage->getColor('random'));
            }
            //imagestringup($stage->context,5 , $px+20 , $py+20 , 'luo hua qiu ci' ,$stage->getColor($color));
        };
    }

    public function filled_circle($centre,$radius=100,$color='')
    {
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$centre,$radius){
            $this->setStartPoint($this->parent?:$stage);
            $cx = $this->stage_x + $this->x + $centre[0];
            $cy = $this->stage_y + $this->y + $centre[1];
            $width = $radius;
            $height = $radius;
            imagefilledellipse($stage->context, $cx, $cy, $width, $height,$stage->getColor($color));
        };
    }

    public function filled_ellipse($centre,$width,$height,$color)
    {
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$centre,$width,$height){
            $this->setStartPoint($this->parent?:$stage);
            $cx = $this->stage_x + $this->x + $centre[0];
            $cy = $this->stage_y + $this->y + $centre[1];
            imagefilledellipse($stage->context, $cx, $cy, $width, $height,$stage->getColor($color));
        };
    }

    public function rectangle($points,$color='',$thickness=1)
    {
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$points,$thickness){
            $this->setStartPoint($this->parent?:$stage);
            $x1 = $this->stage_x + $this->x + $points[0];
            $y1 = $this->stage_y + $this->y + $points[1];
            $x2 = $this->stage_x + $this->x + $points[2];
            $y2 = $this->stage_y + $this->y + $points[3];
            imagesetthickness($stage->context, $thickness);
            imagerectangle($stage->context, $x1, $y1, $x2, $y2,$stage->getColor($color));
        };
    }

    public function filled_rectangle($points,$color='')
    {
        if(!$color){
            $color = $this->color;
        }

        $this->set_list[] = function($stage) use($color,$points){
            $this->setStartPoint($this->parent?:$stage);
            $x1 = $this->stage_x + $this->x + $points[0];
            $y1 = $this->stage_y + $this->y + $points[1];
            $x2 = $this->stage_x + $this->x + $points[2];
            $y2 = $this->stage_y + $this->y + $points[3];
            imagefilledrectangle($stage->context, $x1, $y1, $x2, $y2,$stage->getColor($color));
        };
    }

    public function polygon($points,$color='')
    {
        if(!$color){
            $color = $this->color;
        }
        $this->set_list[] = function($stage) use($color,$points){
            $this->setStartPoint($this->parent?:$stage);

            foreach($points as $k=>$v){
                if($k%2 == 0){
                    $points[$k] = $this->stage_x + $this->x + $v;
                }else{
                    $points[$k] = $this->stage_y + $this->y + $v;
                }
            }

            //imagesetthickness($stage->context, $thickness);
            imageantialias($stage->context, true);
            imagepolygon($stage->context,$points,(int)(count($points)/2),$stage->getColor($color));
        };
    }

    public function pixelsAt($points,$color='',$thickness=1)
    {
        //imagesetpixel(resource img, int x, int y, resource color);

    }

    /**
     * @param $scope 矩形区域 [x,y,w,h]
     * @param $quantity 像素点数量
     * @param string $color
     */
    public function pixelsIn($scope=[],$quantity=100,$color='random')
    {
        if(!$scope){
            $scope = [
                $this->stage_x,
                $this->stage_y,
                $this->parent->width,
                $this->parent->height
            ];
        }

        $color = $this->color;
        //$scop = [x,y,w,h]
        $this->set_list[] = function($stage) use($color,$scope,$quantity){
            $this->setStartPoint($this->parent?:$stage);
            if($this->parent){
                $x1 = $this->stage_x + $this->x + $scope[0];
                $y1 = $this->stage_y + $this->y + $scope[1];
            }else{
                $x1 = $this->x + $scope[0];
                $y1 = $this->y + $scope[1];
            }

            $x2 = $x1 + $scope[2];
            $y2 = $y1 + $scope[3];
            for($i=0;$i<$quantity;$i++){
                imagesetpixel($stage->context, mt_rand($x1,$x2), mt_rand($y1,$y2), $stage->getColor($color));
            }
            //imagestringup($stage->context,5 , $px+20 , $py+20 , 'luo hua qiu ci' ,$stage->getColor($color));
        };
    }

    public function show($stage)
    {
        if($this->parent->type !== 'stage'){

            //根据父级对象，获取（相对于stage的）坐标原点
            $this->stage_x = $this->parent->stage_x + $this->parent->x;
            $this->stage_y = $this->parent->stage_y + $this->parent->y;
        }else{
            $this->stage_x = 0;
            $this->stage_y = 0;
        }
        foreach($this->set_list as $s)
        {
            $s($stage);
        }
    }

}