<?php
namespace Zcg\Legend\Display;

use Zcg\Legend\BitmapData;

class Bitmap extends Display {

    public function __construct(BitmapData $bitmap_data,$x=0,$y=0,$width=0,$height=0)
    {
        parent::__construct();
        $this->content = $bitmap_data;
        $this->x = $x;
        $this->y = $y;
        if($width){
            $this->width = $width;
        }else{
            $this->width = $this->content->clip_width;

        }
        if(!$height){
            $this->height = $this->content->clip_height;
        }else{
            $this->height = $height;
        }

        $this->type = 'bitmap';
    }

    public function show($stage)
    {
        $this->setStartPoint($stage);
        //imagecopy();不具备缩放功能
        imagecopyresampled($stage->context,$this->content->context,$this->stage_x+$this->x,
            $this->stage_y+$this->y,$this->content->clip_x,$this->content->clip_y,$this->width,
            $this->height,$this->content->clip_width,$this->content->clip_height);
        $this->graphic->show($stage);
    }
}