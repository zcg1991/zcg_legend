<?php
namespace Zcg\Legend\Display;

use Hyperf\Utils\Str;

abstract class Display
{
    public static $global_index = 0;//记录全局id最大值，用来生成每个可显示对象的唯一id

    //画布定位点,默认左上角(即设置画布中(0,0)为Display对象的坐标原点)
    public $align = 'top_left';//方位
    public $stage_x = 0;
    public $stage_y = 0;

    public $x=0;//该展示对象在画布中绘制的起点横坐标,相对于画布定位点($stage_x,$stage_y)的横坐标偏移
    public $y=0;//该展示对象在画布中绘制的起点纵坐标,相对于画布定位点($stage_x,$stage_y)的纵坐标偏移
    public $width=0;
    public $height=0;

    public $content;
    public $graphic;//每个可显示对象都包含一个矢量图对象，用来画线条，图形
    public $index = 0;//每一个可展示对象，都应该有一个全局的id
    public $type = 'Display';

    public function __construct()
    {
        $this->graphic = new Graphic($this);
        $this->index = ++self::$global_index;
        if(self::$global_index > 999999){
            self::$global_index = 0;
        }
    }

    /**
     * set coordinate origin
     */
    public function setOrigin($origin)
    {
        if(is_array($origin)) {
            $this->align = 'manual';
            $this->stage_x = $origin[0]??0;
            $this->stage_y = $origin[1]??0;
            return $this;
        }
        //$method = Str::camel($origin);
        $method = lcfirst(str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $origin))));
        if(method_exists($this,$method)) {
            return $this->$method();
        }
        if(class_exists(\Lbxds\Exception\ParamException::class)){
            throw new \Lbxds\Exception\ParamException($origin." 不是合法的对齐方式");
        }
        throw new \Exception($origin." 不是合法的对齐方式");
    }

    /**
     * 沿着某一个方位设置对齐方式：xCenter(横轴居中),y-axis不变
     */
    public function xCenter()
    {
        $this->align = 'x_center';
        return $this;
    }

    //xRight y-axis不变
    public function right()
    {
        $this->align = 'right';
        return $this;
    }

    //xLeft y-axis不变
    public function left()
    {
        $this->stage_x = 0;
        $this->align = 'left';
        return $this;
    }

    //x-axis不变
    public function yCenter()
    {
        $this->align = 'y_center';
        return $this;
    }

    public function top()
    {
        $this->stage_y = 0;
        $this->align = 'top';
        return $this;
    }

    //yBottom x-axis不变
    public function bottom()
    {
        $this->align = 'bottom';
        return $this;
    }

    /**
     * 9个方位设置：上左，上中，上右；中左，中中，中右；下左，下中，下右
     */
    public function center()
    {
        $this->align = 'center';
        return $this;
    }

    public function topLeft()
    {
        $this->stage_x = 0;
        $this->stage_y = 0;
        $this->align = 'top_left';
        return $this;
    }

    public function topCenter()
    {
        $this->stage_y = 0;
        $this->align = 'top_center';
        return $this;
    }

    public function topRight()
    {
        $this->stage_y = 0;
        $this->align = 'top_right';
        return $this;
    }

    public function middleLeft()
    {
        $this->stage_x = 0;
        $this->align = 'middle_left';
        return $this;
    }

    public function middleCenter()
    {
        return $this->center();
    }

    public function middleRight()
    {
        $this->align = 'middle_right';
        return $this;
    }

    public function bottomLeft()
    {
        $this->stage_x = 0;
        $this->align = 'bottom_left';
        return $this;
    }

    public function bottomCenter()
    {
        $this->align = 'bottom_center';
        return $this;
    }

    public function bottomRight()
    {
        $this->align = 'bottom_right';
        return $this;
    }

    /**
     * set starting point coordinate
     */
    public function setCoordinate($x,$y)
    {
        $this->x = $x;
        $this->y = $y;
        return $this;
    }

    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    //获取方位(x_center,right,y_center,bottom等必须添加到画布时才能确定位置)的最终起点坐标
    protected function setStartPoint($stage)
    {
        //get x-center coordinate
        if($this->align === 'x_center' || $this->align === 'top_center' || $this->align === 'bottom_center' || $this->align === 'center'){
            $this->stage_x = ceil(($stage->width - $this->width) / 2);
            if($this->stage_x < 0){
                $this->stage_x = 0;
            }
        }
        //get x-right coordinate
        if($this->align === 'right' || $this->align === 'top_right' || $this->align === 'middle_right' || $this->align === 'bottom_right'){
            $this->stage_x = $stage->width - $this->width;
            if($this->stage_x < 0){
                $this->stage_x = 0;
            }
        }
        //get y-bottom coordinate
        if($this->align === 'bottom' || $this->align === 'bottom_left' || $this->align === 'bottom_center' || $this->align === 'bottom_right'){
            $this->stage_y = $stage->height - $this->height;
            if($this->stage_y < 0){
                $this->stage_y = 0;
            }
        }
        //get y-center coordinate
        if($this->align === 'y_center' || $this->align === 'center' || $this->align === 'middle_left' || $this->align === 'middle_right'){
            $this->stage_y = ceil(($stage->height - $this->height)/2);
            if($this->stage_y < 0){
                $this->stage_y = 0;
            }
        }
    }

    abstract public function show($stage);
}