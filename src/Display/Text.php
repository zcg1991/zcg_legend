<?php
namespace Zcg\Legend\Display;

/**
 * Class Text
 * 基础可显示对象，只能设置相对于stage的起点位置，宽高随着内容自动调整，不可强制设置宽高
 * 基础可显示对象包括：Text文本,Bitmap位图,Graphic图形
 */
class Text extends Display
{
    public $size;
    public $angle;
    public $color;
    public $font;
    public $font_width;
    public $font_height;
    public $text_align;//字体特殊的对齐方式 equal_spacing:等间隔
    public $relative_stage=false;//eqaul_spacing等对齐方式时，默认使用stage宽度去平均分配
    public $length=0;//eqaul_spacing的总长度
    public $direction = 'horizontal';
    public $fixed_y = 0;//由于imagettfbbox导致的y轴偏移
    public $line_width = 0;//换行长度，0表示不换行
    public $weight = 0;//字体加粗,1表示加粗已被,3表示三倍
    public $space = null;//每个字之间的间隔
    public $line_height = 1;//换行间距

    public function __construct($content,$color='black',$size = 24,$angle=0,$font='',$text_align='',$line_width=0,$weight=0,$space=null,$line_height=1)
    {
        if($font){
            if($font === 'simli'){
                $this->font = dirname(__FILE__,3).'/simli.ttf';
            }else if($font === 'msyh'){
                $this->font = dirname(__FILE__,3).'/msyh.ttc';
            }else if($font === 'pingfang'){
                $this->font = dirname(__FILE__,3).'/pingfang.ttf';
            }else if($font === 'arial'){
                $this->font = dirname(__FILE__,3).'/arial.ttf';
            }else if($font === 'alihuipu'){
                $this->font = dirname(__FILE__,3).'/alihuipu.ttf';
            }else{
                $this->font = dirname(__FILE__,3).'/helvetica.ttf';
            }
        }else{
            $this->font = dirname(__FILE__,3).'/simli.ttf';//默认隶书字体
        }

        $this->size = $size;
        $this->angle = $angle;
        $this->content = $content;
        $this->color = $color;
        $this->type = 'text';
        $this->text_align = $text_align;
        $this->line_width = $line_width;
        //头二个分别为左下的 x、y 坐标，第三、四个为右下角的 x、y 坐标，第五、六及七、八二组分别为右上及左上的 x、y 坐标
        $this->box = imagettfbbox($this->size, $this->angle, $this->font, $this->content);
        $this->width = $this->box[2] - $this->box[0];
        $this->height = $this->box[1] - $this->box[7];
        $this->font_width = $this->width;
	$this->font_height = $this->height;
	$this->weight = $weight;
	$this->space = $space;
        $this->line_height = $line_height;
        if(strstr($this->font,'simli')){
            $this->fixed_y = $this->box[7];
        }
        if(strstr($this->font,'msyh')){
            $this->fixed_y = $this->box[7]/2;
        }

        if($this->fixed_y < 0){
            $this->y += abs($this->fixed_y);
        }else{
            $this->y -= abs($this->fixed_y);
        }

//        if($this->box[7] < 0){
//            $this->y += abs($this->box[7]);
//        }else{
//            $this->y -= abs($this->box[7]);
//        }
        parent::__construct();
    }

    /**
     * set starting point coordinate
     */
    public function setCoordinate($x,$y)
    {
        $this->x = $x;
        $this->y = $y - $this->fixed_y;
        return $this;
    }

    public function setY($y)
    {
        $this->y = $y - $this->fixed_y;
        return $this;
    }

    public function equalSpacing(int $length=-1)
    {
        if($length == -1){
            $this->relative_stage = true;//相对于舞台
        }else{
            $this->length = $length;//设定字体所占总宽度，其余空间按等间隔
        }

        $this->text_align = 'equal_spacing';
        return $this;
    }

    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    //获取字体数组
    public function getTextArr()
    {
        mb_internal_encoding("UTF-8"); // 设置编码
        $arr = [];
        $total_width = 0;
        for ($i=0;$i<mb_strlen($this->content);$i++) {
            $char = mb_substr($this->content, $i, 1);
            $test = imagettfbbox($this->size, $this->angle, $this->font, $char);
            $total_width += $test[2] - $test[0];

            $arr[] = [
                'char' => $char,
                'width' => $test[2] - $test[0],
                'total_width' => $total_width,
                'height' => $test[1] - $test[7]
            ];
        }
        return $arr;
    }

    //添加换行符
    public function lineFeed()
    {
        if($this->line_width){
            $content = '';
            $arr = $this->getTextArr();
            foreach ($arr as $l) {
                $teststr = $content." ".$l;
                $fontBox = imagettfbbox($this->size, $this->angle, $this->font, $teststr);
                if (($fontBox[2] > $this->line_width) && ($content !== "")) {
                    $content .= "\n\n";
                }
                $content .= $l;
            }
            $this->content = $content;
        }
    }

    public function show($stage)
    {
        $len = mb_strlen($this->content);

        if($this->direction === 'horizontal'){
            if($this->length > 0){
                $this->width = $this->length;
            }
        }else{
            if($this->length > 0){
                $this->height = $this->length;
            }
            $this->width =  floor($this->font_width/$len);
        }

        $this->setStartPoint($stage);//有些定位只有在最终添加到舞台时才能确定

        $arr = $this->getTextArr();

        if($this->direction === 'horizontal'){
            if ($this->relative_stage) {
                //在stage
                $w = $stage->width;
                $x = 0;
            } else {
                //先对于自身
                $w = $this->width;
                $x = $this->stage_x + $this->x;
            }
	    if($this->space !== null){
                $space = $this->space;
	    }else{
                if($this->text_align === 'equal_spacing') { //主要用于生成验证码等，等距离文字写入
                    $space = floor(($w - $this->font_width)/($len + 1));
                }else{
                    $space = 1;
	        }
	    }

            $delta = 0;
            $first_3 = false;
            $first_2 = false;
            $first_1 = false;
            for($i=0;$i<$len;$i++) {
                $cur_y = $this->stage_y + $this->y;
                //换行处理
                if($this->line_width && $this->text_align !== 'equal_spacing'){
                    if($space*($i+1)+$arr[$i]['total_width']>=3*$this->line_width){
                        $cur_y =  $this->stage_y + $this->y + 3*($this->font_height + $this->line_height);
                        if(!$first_3){
                            $delta = $arr[$i-1]['total_width']+$i*$space;
                        }
                        $first_3 = true;
                    }else if($space*($i+1)+$arr[$i]['total_width']>=2*$this->line_width){
                        $cur_y =  $this->stage_y + $this->y + 2*($this->font_height + $this->line_height);
                        if(!$first_2){
                            $delta = $arr[$i-1]['total_width']+$i*$space;
                        }
                        $first_2 = true;
                    }else if($space*($i+1)+$arr[$i]['total_width']>=$this->line_width){
                        $cur_y =  $this->stage_y + $this->y + $this->font_height + $this->line_height;
                        if(!$first_1){
                            $delta = $arr[$i-1]['total_width']+$i*$space;
                        }
                        $first_1 = true;
                    }
                }
                $offset_font = $i>0?$arr[$i-1]['total_width']:0;
                imagettftext($stage->context, $this->size, $this->angle, $x+$space*($i+1)+$offset_font-$delta, $cur_y, $stage->getColor($this->color), $this->font, $arr[$i]['char']);
		if($this->weight > 0){
		    for($k=0;$k<=$this->weight;$k++){
			imagettftext($stage->context, $this->size, $this->angle, $k+$x+$space*($i+1)+$offset_font-$delta, $cur_y, $stage->getColor($this->color), $this->font, $arr[$i]['char']);
		    }
		}
            }
        }else{
            if($this->text_align === 'equal_spacing') { //主要用于生成验证码等，等距离文字写入
                if($this->relative_stage){
                    //在stage
                    $h = $stage->height;
                    $y = 0-$this->fixed_y;
                }else{
                    //相对于自身
                    $h = $this->height;
                    $y = $this->stage_y + $this->y;
                }
                $space = floor(($h - $this->font_height*$len)/($len + 1));
            }else{
                $space = 1;
            }

            $single_height = $this->font_height;
            for($i=0;$i<$len;$i++) {
                $str = mb_substr($this->content,$i,1);
                imagettftext($stage->context, $this->size, $this->angle, $this->stage_x + $this->x,$y+$space*($i+1)+$this->font_height*$i,$stage->getColor($this->color), $this->font, $str);
            }
        }

//            $this->lineFeed();
//            if($this->direction === 'vertical'){
//                $content = preg_replace('/(.)/u', "$1\n", $this->content);
//                imagettftext($stage->context, $this->size, $this->angle, $this->stage_x + $this->x, $this->stage_y + $this->y, $stage->getColor($this->color), $this->font, $content);
//            }else{
//                imagettftext($stage->context, $this->size, $this->angle, $this->stage_x + $this->x, $this->stage_y + $this->y, $stage->getColor($this->color), $this->font, $this->content);
//            }


        $this->graphic->show($stage);
    }
}
