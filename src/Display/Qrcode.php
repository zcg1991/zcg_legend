<?php
namespace Zcg\Legend\Display;

use Zcg\Legend\BitmapData;

class Qrcode extends Display
{
    private $file;
    private $level;//0-3 容错级别递减,0容错最好
    private $size;//3,4,5
    private $margin;//1,2

    public function __construct($content,$level=0,$size=5,$margin=2)
    {
        parent::__construct();
        $this->type = 'qrcode';

        require_once dirname(__FILE__,3).'/phpqrcode.php';
        if(!file_exists(BASE_PATH.'/runtime/images')){
            mkdir(BASE_PATH.'/runtime/images',0777,true);
        }

        $this->file = BASE_PATH.'/runtime/images/'.time().mt_rand(10000,99999);
        $this->level = $level;
        $this->size = $size;
        $this->margin = $margin;

        $this->content = $content;
    }

    public function setSize($size){
        $this->size = $size;
        return $this;
    }

    public function getFile()
    {
        \QRcode::png($this->content,$this->file.'.png',$this->level,$this->size,$this->margin);
        return $this->file;
    }


    public function show($stage)
    {
        $this->setStartPoint($stage);//有些定位只有在最终添加到舞台时才能确定
        $this->graphic->show($stage);
    }
}