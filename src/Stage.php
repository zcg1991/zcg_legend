<?php
namespace Zcg\Legend;

use Zcg\Legend\Display\Graphic;

class Stage{

    public $width;
    public $height;
    public $context;
    public $graphic;
    public $type = 'stage';

    private $children = [];
    private $has_show = false;

    public function __construct($width,$height,$background='white',$alpha=0)
    {
        $this->width = $width;
        $this->height = $height;
        $this->context = imagecreatetruecolor($width, $height);
        if($alpha){
            imagesavealpha($this->context, true);
            imagefill($this->context,0,0,$this->getColor($background,$alpha));
//            imagecolortransparent($this->context, $this->getColor($background,$alpha));//将背景色$this->getColor($background)变为透明色
        }else{
            imagefill($this->context,0,0,$this->getColor($background));
        }
        $this->graphic = new Graphic($this);
    }

    //only stage or sprite can create color,$alpha=0不透明,$alpha=127完全透明
    public function getColor($color,$alpha=0)
    {
        if(is_array($color)){
            if($alpha){
                return imagecolorallocatealpha($this->context, $color[0], $color[1], $color[2],$alpha);
            }
            return imagecolorallocate($this->context, $color[0], $color[1], $color[2]);
        }else{
            switch ($color) {
                case 'white':
                    return $this->getColor([255,255,255],$alpha);
                case 'black':
                    return $this->getColor([0,0,0],$alpha);
                case 'red':
                    return $this->getColor([255,0,0],$alpha);
                case 'green':
                    return $this->getColor([0,255,0],$alpha);
                case 'yellow':
                    return $this->getColor([255,255,0],$alpha);
                case 'blue':
                    return $this->getColor([0,0,255],$alpha);
                case 'purple':
                    return $this->getColor([128,0,128],$alpha);
                case 'orange':
                    return $this->getColor([255,97,0],$alpha);
                case 'white_red':
                    return IMG_COLOR_STYLED;//填充一个矩形,imagesetstyle时特殊填充时使用
                default:
                    return $this->getColor([mt_rand(0,255),mt_rand(0,255),mt_rand(0,255)]);
            }
        }
    }

    public function add_child($child)
    {
        $this->children[] = $child;
        return $this;
    }

    public function add_child_at($child,$sort)
    {
        if($sort < 1 || $sort > count($this->children)+1){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('插入节点位置错误');
            }
            throw new \Exception('插入节点位置错误');
        }
        for($i = count($this->children)-1;$i>=$sort-1;$i--)
        {
            $this->children[$i+1] = $this->children[$i];
        }
        $this->children[$sort-1] = $child;
        return $this;
    }

    public function sort_child($child,$sort)
    {
        if($sort < 1 || $sort > count($this->children)){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('设置排序位置错误');
            }
            throw new \Exception('设置排序位置错误');
        }
        $old_sort = $this->find_child_position($child);
        $new_sort = $sort-1;
        if($old_sort == -1 || $old_sort == $new_sort){
            if(class_exists(\Lbxds\Exception\ParamException::class)){
                throw new \Lbxds\Exception\ParamException('位置相同无需排序');
            }
            throw new \Exception('位置相同无需排序');
        }
        $tmp = $this->children[$old_sort];
        if($new_sort > $old_sort){
            //向后排序（原来的位置在前面,需前移元素腾出位置）
            for($i = $old_sort;$i < $new_sort;$i++)
            {
                $this->children[$i] = $this->children[$i+1];
            }
        }else{
            //靠前排序（原来的位置在后面,需后移元素腾出位置）
            for($i = $old_sort;$i>$new_sort;$i--)
            {
                $this->children[$i] = $this->children[$i-1];
            }
        }
        $this->children[$new_sort] = $tmp;
        return $this;
    }

    public function remove_child($child)
    {
        $position = $this->find_child_position($child);
        if($position >= 0){
            for($i = $position;$i<count($this->children)-1;$i++)
            {
                $this->children[$i] = $this->children[$i+1];
            }
            unset($this->children[$i]);
        }
        return $this;
    }

    public function remove_children($children)
    {
        foreach($children as $child)
        {
            $this->remove_child($child);
        }
        return $this;
    }

    public function clear_child()
    {
        $this->children = [];
        return $this;
    }

    public function find_child_position($child)
    {
        foreach ($this->children as $key => $value) {
            if($value->index == $child->index){
                return $key;
            }
        }
        return -1;
    }

    public function traversal()
    {
        foreach ($this->children as $key => $value) {
            var_dump('index:'.$value->index.'===type:'.$value->type);
            echo "<br>";
        }
        echo "<hr>";
    }

    public function save($prefix='',$filename='',$path='')
    {
        if(!$path) {
            if(!file_exists(BASE_PATH.'/runtime/images')){
                mkdir(BASE_PATH.'/runtime/images');
            }
            $path = BASE_PATH.'/runtime/images/';
        }
        if(!$filename){
            $filename = time().mt_rand(10000,99999);
        }
        $filename = $path.$prefix.$filename;
        if(!$this->has_show){
            $this->show();
        }

        imagepng($this->context,$filename.'.png');
        imagedestroy($this->context);
        return $filename.'.png';
    }

//    public function base64()
//    {
//        if(!$this->has_show){
//            $this->show();
//        }
//        ob_start();
//        imagepng($this->context);
//        $image_data = ob_get_contents();
//        ob_end_clean ();
//        imagedestroy($this->context);
//        return $image_data;
//    }

    public function browser()
    {
        header("Content-Type:image/png;charset=utf-8");
        if(!$this->has_show){
            $this->show();
        }
        imagepng($this->context);
        imagedestroy($this->context);
    }

    public function show()
    {
        foreach ($this->children as $child){
            $child->show($this);
        }
        $this->graphic->show($this);
        $this->has_show = true;
    }

//    protected function showChildren($canvas)
//    {
//        foreach ($canvas->children as $child){
//            if($child->type === 'sprite') {
//                if($child->children){
//                    $this->showChildren($child);
//                    imagedestroy($child->context);
//                }
//            }else{
//                $child->show($canvas);
//            }
//        }
//    }
}
