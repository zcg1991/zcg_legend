<?php

function zBarcode($code){
    $barcode = new \Zcg\Legend\App\BarCode($code);
    return $barcode->createBarCode();
}

//$level:0-3;$size:3,4,5;$margin:1,2
function zQrcode($content,$is_base64=false,$logo='',$size=5,$margin=1,$level=0)
{
    require_once dirname(__FILE__,2).'/phpqrcode.php';
    if(!file_exists(BASE_PATH.'/runtime/images')){
        mkdir(BASE_PATH.'/runtime/images',0777,true);
    }
    $filename = BASE_PATH.'/runtime/images/'.time().mt_rand(10000,99999).'.png';
    \QRcode::png($content,$filename,$level,$size,$margin);
    if(!$logo){
        deliver(new Lbxds\Job\DeleteFileJob($filename),300);
        if($is_base64){
            return base64_encode(file_get_contents($filename));
        }
        return $filename;
    }
    $qr_bd = new \Zcg\Legend\BitmapData($filename);
    $logo_bd = new \Zcg\Legend\BitmapData($logo);
    $qr_bp = new \Zcg\Legend\Display\Bitmap($qr_bd);
    $stage = new \Zcg\Legend\Stage($qr_bd->width,$qr_bd->height);
    $w = $stage->width / 5;
    $h = $stage->height * $w / $stage->width;
    $logo_bp = new \Zcg\Legend\Display\Bitmap($logo_bd,0,0,$w,$h);//缩放图片
    $logo_bp->center();
    $stage->add_child($qr_bp);
    $stage->add_child($logo_bp);
    $file = $stage->save();
    if($is_base64){
        deliver(new Lbxds\Job\DeleteFileJob($file),300);
        return base64_encode(file_get_contents($file));
    }
    return  $file;
}

function zCode($arr=[])
{
    $width = $arr['width'] ?? 200;
    $height = $arr['height'] ?? 50;
    $background = $arr['background'] ?? 'white';
    $color = $arr['color'] ?? 'random';
    $size = $arr['size'] ?? 24;
    $angle = $arr['angle'] ?? 0;
    $text_align = $arr['text_align'] ?? 'equal_spacing';
    $pixels = $arr['pixels'] ?? 0;
    $circles = $arr['circles'] ?? 0;
    $transparent = $arr['transparent'] ?? 0;
    $amount = $arr['amount'] ?? 4;
    if(function_exists('genRandom')){
        $str = genRandom($amount);
    }else{
        $arr = array_merge(range(2,9),range('a','z'),range('A','Z'));
        $get_arr = array_rand($arr,$amount);
        $str = '';
        foreach ($get_arr as $k){
            $str .= $arr[$k];
        }
    }

    $stage = new \Zcg\Legend\Stage($width, $height, $background,$transparent);
    $text = new \Zcg\Legend\Display\Text($str, $color, $size, $angle,'',$text_align);
    $text->center();
    $text->equalSpacing();
    $stage->add_child($text);
    $stage->graphic->pixelsIn([],$pixels);
    $stage->graphic->circlesIn([],$circles);
    $file = $stage->save('verify_code_','code');
    return ['file'=>$file,'code'=>$str];
}

//给所有的坐标添加偏移值
function getPoints($points,$offset_x,$offset_y,$dimension=1)
{
    //[5,5,5,10]
    if($dimension == 1){
        $bore = [];
        foreach ($points as $k=>$v) {
            if($k % 2 == 0){
                $bore[] = $v+$offset_x;
            }else{
                $bore[] = $v+$offset_y;
            }
        }
        return $bore;
    }

    //[[5,5],[5,10]]
    if($dimension == 2)
    {
        $bounce = [];
        foreach ($points as $k => $v) {
            $bounce[] = [
                $v[0]+$offset_x,
                $v[1]+$offset_y
            ];
        }
        return $bounce;
    }

    return $points;
}

//获取当前颜色的反差最大的颜色
function color_reverse($rgb)
{
    foreach ($rgb as &$c){
        $c = $c /255;
        if($c <= 0.03928){
            $c = $c / 12.92;
        }else{
            $c = pow(($c+0.055)/1.055,2.4);
        }

    }
    if( 0.2126 * $rgb[0] + 0.7152 * $rgb[1] + 0.0722 * $rgb[2] > 0.179){
        return [0,0,0];
    }else{
        return [255,255,255];
    }
}
